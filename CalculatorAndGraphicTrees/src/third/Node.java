package third;

public class Node {

	String data = "";
	Node root;
	Node left;
	Node right;

	public Node(String data) {
		this.data = data;
		this.left = null;
		this.right = null;
		this.root = null;
	}

	public Node(Node op1, String pop, Node op2) {
		this.right = op1;
		this.data = pop;
		this.left = op2;
		this.root = null;

	}

	public Node getroot() {
		return root;
	}

	public void setroot(Node root) {
		this.root = root;
	}

	public String getdata() {
		return data;
	}

	public void setdata(String data) {
		this.data = data;
	}

	public Node getleft() {
		return left;
	}

	public void setleft(Node left) {
		this.left = left;
	}

	public Node getright() {
		return right;
	}

	public void setright(Node right) {
		this.right = right;
	}


	private static String travel = "";
	
	static void clean() {
		travel = "";
	}
	
	void inOrder(Node node) {
		if(node != null){
			inOrder(node.left);
			travel += node.data + " ";
			inOrder(node.right);
		}
	}
	
	String inOrder() {
		inOrder(root);
		return travel;
	}
}
