package third;
public class AltStack {

    private NodeStack top;

    boolean isEmpty() {
        return top == null;
    }

    void push(String data) {
        NodeStack node = new NodeStack(data);
        node.next = top;
        top = node;
    }
    
    void push2(NodeStack node) {
        //NodeStack node = new NodeStack(data);
        node.next = top;
        top = node;
    }

    String pop() {
        String value = null;
        if (isEmpty()) {
            //System.out.println("The stack is empty");
        } else {
            String aux = top.data;
            top = top.next;
            //System.out.println(aux);
            value = aux;
        }
        return value;
    }

    void list(){
        if (isEmpty()) {
            //System.out.println("The stack is empty");
        } else {
            NodeStack temp = top;
            while (temp != null){
                //System.out.println("% " + temp.data);
                temp = temp.next;
            }
        }
    }
    
    String latest() {
        if (isEmpty()) {
            return null;
        } else {
            return top.data;
        }
    }
}

class NodeStack {

    NodeStack next;
    String data;

    NodeStack(String data) {
        this.data = data;
    }
}
