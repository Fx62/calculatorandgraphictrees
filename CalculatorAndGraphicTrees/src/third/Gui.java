package third;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.io.File;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Gui extends javax.swing.JFrame {

	static String[][] matrix = null;
	private static JPanel contentPane;
	private Simulator simulador = new Simulator();
	static String draw;
	private static boolean execute = true;
	private static boolean drawTree = false;

	public Gui() {
		initComponents();
		this.pack();

	}

	@SuppressWarnings("unchecked")
	private void initComponents() {
		inputFile();
		if(drawTree) {
			jPanel2 = new javax.swing.JPanel();
			jPanel3 = new javax.swing.JPanel();
			botonInsertar = new javax.swing.JButton();
			// jButton1 = new javax.swing.JButton();
			jDesktopPane1 = new javax.swing.JDesktopPane();
			jInternalFrame2 = new javax.swing.JInternalFrame();

			setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
			setResizable(false);
			jPanel2.setOpaque(false);
			jPanel3.setOpaque(false);
			botonInsertar.setText("Dibujar arbol");
			botonInsertar.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					botonInsertarActionPerformed(evt);
				}
			});

			javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
			jPanel3.setLayout(jPanel3Layout);
			jPanel3Layout
					.setHorizontalGroup(
							jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
									.addGroup(jPanel3Layout.createSequentialGroup().addGap(60, 60, 60)
											// .addComponent(jButton1).addGap(139, 139, 139)
											.addComponent(botonInsertar, javax.swing.GroupLayout.PREFERRED_SIZE, 200,
													javax.swing.GroupLayout.PREFERRED_SIZE)
											.addContainerGap(387, Short.MAX_VALUE)));
			jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
							jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
									.addComponent(botonInsertar, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)));

			jDesktopPane1.setOpaque(false);
			javax.swing.GroupLayout jInternalFrame2Layout = new javax.swing.GroupLayout(jInternalFrame2.getContentPane());
			jInternalFrame2.getContentPane().setLayout(jInternalFrame2Layout);
			jInternalFrame2Layout.setHorizontalGroup(jInternalFrame2Layout
					.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 884, Short.MAX_VALUE));
			jInternalFrame2Layout.setVerticalGroup(jInternalFrame2Layout
					.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 340, Short.MAX_VALUE));

			jDesktopPane1.add(jInternalFrame2);
			jInternalFrame2.setBounds(0, 0, 1000, 470);

			javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
			jPanel2.setLayout(jPanel2Layout);
			jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(jPanel2Layout.createSequentialGroup().addContainerGap()
							.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
									.addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE,
											javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(jDesktopPane1))
							.addContainerGap()));
			jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(jPanel2Layout.createSequentialGroup().addContainerGap()
							.addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 339,
									javax.swing.GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
							.addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE,
									javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
							.addContainerGap()));

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
			getContentPane().setLayout(layout);
			layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(layout.createSequentialGroup().addContainerGap().addComponent(jPanel2,
							javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addContainerGap()));
			layout.setVerticalGroup(
					layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
									layout.createSequentialGroup()
											.addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE,
													javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addContainerGap()));

			jPanel2.getAccessibleContext().setAccessibleName("panel de pruebas");
			jPanel2.getAccessibleContext().setAccessibleDescription("");

			pack();
		} else {
			JOptionPane.showMessageDialog(null, "No existe instruccion para dibujar arbol", 
					"Informacion", JOptionPane.DEFAULT_OPTION);
			System.exit(0);
		}
	}

	private void botonInsertarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_botonInsertarActionPerformed
		try {
			if (this.simulador.insertar(draw)) {
				complementos();
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "No existe arbol para ser mostrado", "Error", 0);
		}
	}

	public void complementos() {
		this.repintarArbol();
	}

	private void repintarArbol() {
		this.jDesktopPane1.removeAll();
		Rectangle tamaño = this.jInternalFrame2.getBounds();
		this.jInternalFrame2 = null;
		this.jInternalFrame2 = new JInternalFrame("", true);
		this.jDesktopPane1.add(this.jInternalFrame2, JLayeredPane.DEFAULT_LAYER);
		this.jInternalFrame2.setVisible(true);
		this.jInternalFrame2.setBounds(tamaño);
		this.jInternalFrame2.setEnabled(false);
		this.jInternalFrame2.add(this.simulador.getDibujo(), BorderLayout.CENTER);
	}

	public static void main(String args[]) {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(Gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(Gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(Gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(Gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}

		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Gui().setVisible(true);
			}
		});
	}

	private javax.swing.JButton botonInsertar;
	private javax.swing.JDesktopPane jDesktopPane1;
	private javax.swing.JInternalFrame jInternalFrame2;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JPanel jPanel3;

	public static File chooser() {
		JOptionPane.showMessageDialog(null, /*
											 * "Puntos a tomar en cuenta: \n" +
											 * "   1.- Las operaciones a realizar no deben de contener espacios\n" +
											 * "   2.- Unicamente una operacion debe de ser asignada por linea\n" +
											 * "   3.- La funcion valuar() debe de contener dentro del parentesis " +
											 * "la variable a calcular\n" +
											 * "   4.- La funcion dibujar() debe de contener dentro del parentesis " +
											 * "la variable a calcular\n" +
											 * "   5.- Para calcular el valor de una raiz cuadrada se realiza por medio de"
											 * + "_() con el nombre de la variable dentro de los parentesis\n" + "\n\n
											 */"Por favor seleccione el archivo");
		JFileChooser fc = new JFileChooser();
		File file = null;
		int seleccion = fc.showOpenDialog(contentPane);
		if (seleccion == JFileChooser.APPROVE_OPTION) {
			try {
				return fc.getSelectedFile();
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null, "Archivo invalido");
			}
		} else {
			JOptionPane.showMessageDialog(null, "Instruccion invalida", "Error", JOptionPane.DEFAULT_OPTION);
			execute = false;
		}
		return file;
	}

	public static void inputFile() {
		try {
			File file = chooser();
			if (execute) {
				Scanner sc = new Scanner(file);
				int linesfile = 0;
				while (sc.hasNextLine()) {
					if (sc.nextLine().contains("=")) {
						linesfile++;
					}
				}
				sc.close();
				matrix = new String[linesfile][5];
				sc = new Scanner(file);
				int cont = 0;
				int cont2 = 0;
				while (sc.hasNextLine()) {
					cont++;
					String line = sc.nextLine();
					if (line.contains("=")) {
						//JOptionPane.showMessageDialog(null, "El archivo contiene lineas invalidas", "Error",
						//JOptionPane.DEFAULT_OPTION);
						System.out.print(testOperation(line, cont2) ? "" : "Linea " + cont + " invalida\n");
						cont2++;
					} else {
						if (line.contains("(") && line.contains(")")) {
							String function = line.substring(0, line.indexOf("("));
							String find = line.substring(line.indexOf("(") + 1, line.indexOf(")"));
							if (function.equals("valuar")) {
								for (int i = 0; i < matrix.length; i++) {
									if (matrix[i][0].equals(find)) {
										try {
											if (matrix[i][1].equals(null)) {
												JOptionPane.showMessageDialog(null,
														"No fue posible encontrar un resultado"
																+ "valido para la funcion: " + matrix[i][0]);
											} else {
												File file2 = new File(file.getAbsolutePath() + "_outputValue.txt");
												Formatter f = new Formatter(file2);
												f.format("Valuar %s\n", matrix[i][0]);
												f.format("%s=%s\n", matrix[i][0], matrix[i][1]);
												JOptionPane.showMessageDialog(null,
														matrix[i][0] + "=" + matrix[i][1] +
														"\nResultado guardado en "
																+ "archivo: " + file2.getCanonicalPath());
												f.close();
											}
										} catch (Exception e) {
											JOptionPane.showMessageDialog(null, "No fue posible guardar el archivo");
										}
									}
								}
							} else if (function.equals("dibujar")) {
								for (int i = 0; i < matrix.length; i++) {
									if (matrix[i][0].equals(find)) {
										if (matrix[i][3].equals(null)) {
											JOptionPane.showMessageDialog(null, "No fue posible encontrar un resultado"
													+ "valido para la funcion: " + matrix[i][0]);
										} else {
											String[] temp = matrix[i][3].split(" ");
											draw = matrix[i][4];
											drawTree = true;
											System.out.println(draw);
										}
									}
								}
							} else {
								System.out.println("Linea " + cont + " invalida");
								break;
							}
						}
					}
				}
				sc.close();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static boolean testOperation(String operation, int i) {
		String[] temp = null;
		temp = operation.split("=");
		if (temp.length == 2) {
			if (temp[0].length() == 1) {
				String result = "";
				result += temp[0];
				// result += temp[1];
				try {
					// System.out.println(operation);
					matrix[i][0] = result;
					String postfix = postfix(returnOperation(temp[1]));
					// System.out.println(postfix);
					matrix[i][1] = evaluatePostfix(postfix);
					// result += evaluatePostfix(postfix);
					matrix[i][2] = postfix;
					// System.out.println(result);
					// System.out.println(pre2post(postfix));
					matrix[i][3] = pre2post(postfix);
					matrix[i][4] = temp[1];
				} catch (Exception e) {
					// System.out.println(postfix(temp[1]));
				}
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public static byte precedence(String value) {
		byte p1 = 0;
		switch (value) {
		case "(":
			p1 = 0;
			break;
		case ")":
			p1 = 0;
			break;
		case "[":
			p1 = 0;
			break;
		case "]":
			p1 = 0;
			break;
		case "^":
			p1 = 3;
			break;
		case "_":
			p1 = 3;
			break;
		case "*":
			p1 = 2;
			break;
		case "/":
			p1 = 2;
			break;
		case "+":
			p1 = 1;
			break;
		case "-":
			p1 = 1;
			break;
		}
		return p1;
	}

	public static String postfix(String original) {
		AltStack stack = new AltStack();
		String result = "";
		byte actualPrecedence = 0;
		byte beforePrecedence = 0;
		String tempString;
		for (String t : fillStack(original)) {
			try {
				Integer.parseInt(t);
				result += t + " ";
			} catch (Exception e) {
				actualPrecedence = precedence(t);
				if (actualPrecedence != 6) {
					if (stack.isEmpty()) {
						if (t.equals(")") || t.equals("]")) {
							break;
						} else {
							beforePrecedence = precedence(t);
							stack.push(t);
						}
					} else {
						if (actualPrecedence == 0) {
							if (t.equals("(") || t.equals("[")) {
								stack.push(t);
								beforePrecedence = actualPrecedence;
							} else {
								tempString = stack.pop();
								while (!tempString.equals(null) && !tempString.equals("(") && !tempString.equals("[")) {
									result += tempString + " ";
									tempString = stack.pop();
									beforePrecedence = precedence(tempString);
								}
								tempString = stack.latest();
							}
						} else {
							if (actualPrecedence > beforePrecedence) {
								if (beforePrecedence == 0) {
									stack.push(t);
									beforePrecedence = actualPrecedence;
								} else {
									stack.push(t);
									beforePrecedence = actualPrecedence;
								}
							} else {
								if (actualPrecedence == beforePrecedence) {
									result += stack.pop() + " ";
									stack.push(t);
									beforePrecedence = actualPrecedence;
								} else {
									result += stack.pop() + " ";
									stack.push(t);

									beforePrecedence = actualPrecedence;
								}
							}
						}
					}
				} else {
					return "La operacion ingresada no es valida";
				}
			}
			stack.list();
		}
		while (!stack.isEmpty()) {
			result += stack.pop() + " ";
		}
		return result;
	}

	public static String returnOperation(String original) {
		String result = "";
		boolean control2 = false;
		for (String t : fillStack(original)) {
			try {
				Integer.parseInt(t);
				result += t + " ";
			} catch (Exception e) {
				try {
					char tempChar = t.charAt(0);
					if (Character.isLetter(tempChar)) {
						for (int i = 0; i < matrix.length; i++) {
							if (matrix[i][0].contains(t)) {
								original = original.replaceAll(t, matrix[i][1]);
								control2 = check(original);
								if (control2) {
									return original;
								}
							}
						}
					} else {
						if (check(original)) {
							return original;
						}
					}
				} catch (Exception f) {

				}
			}
		}
		return result;
	}

	public static String pre2post(String pre) {
		if (pre.length() <= 1) {
			return pre;
		}

		if (!Character.isLetter(pre.charAt(0))) {
			String a = pre2post(pre.substring(1)) + pre.charAt(0);
			String b = pre2post(pre.substring(a.length()));
			return a + b;
		} else if (!Character.isLetter(pre.charAt(1))) {
			return pre.substring(0, 1);
		} else {
			return pre.substring(0, 2);
		}

	}

	public static boolean check(String operation) {
		boolean control = true;
		for (int i = 0; i < operation.length(); i++) {
			String temp = String.valueOf(operation.charAt(i));
			try {
				Integer.parseInt(temp);
			} catch (Exception e) {
				if (!temp.equals("+") && !temp.equals("-") && !temp.equals("*") && !temp.equals("/")
						&& !temp.equals("(") && !temp.equals(")") && !temp.equals("[") && !temp.equals("]")
						&& !temp.equals("^") && !temp.equals("_")) {
					return false;
				} else {
					control = true;
				}
			}
		}
		return control;
	}

	public static String[] fillStack(String original) {
		ArrayList<String> temp = new ArrayList<String>();
		String symbols = "";
		int tempNumber;
		String tempSymbol;
		String auxiliarString = "";
		for (int i = 0; i < original.length(); i++) {
			symbols = "";
			tempSymbol = String.valueOf(original.charAt(i));
			try {
				tempNumber = Integer.parseInt(tempSymbol);
				auxiliarString += String.valueOf(tempNumber);
				try {
					Integer.parseInt(String.valueOf(original.charAt(i + 1)));
				} catch (Exception e) {
					temp.add(auxiliarString);
					auxiliarString = "";
				}

			} catch (Exception e) {
				symbols += tempSymbol;
				temp.add(symbols);
			}
		}
		String[] fix = new String[temp.size()];
		for (int i = 0; i < temp.size(); i++) {
			fix[i] = temp.get(i);
		}
		return fix;
	}

	static String evaluatePostfix(String exp) {
		AltStack stack = new AltStack();
		// String tempString = "";
		String[] temp = exp.split(" ");
		// System.out.println(exp);
		// for (String t : temp) {
		// System.out.print(t + " ");
		// }
		// Scan all characters one by one
		for (int i = 0; i < temp.length; i++) {
			String c = temp[i];
			// If the scanned character is an operand (number here),
			// push it to the stack.
			try {
				Integer.parseInt(c);
				// System.out.println(tempString);
				stack.push(c);
			} // If the scanned character is an operator, pop two
				// elements from stack apply the operator
			catch (Exception e) {
				int val1 = 0;
				int val2 = 0;
				try {
					val1 = Integer.parseInt(stack.pop());
					val2 = Integer.parseInt(stack.pop());
					switch (c) {
					case "+":
						stack.push(String.valueOf(val2 + val1));
						break;

					case "-":
						stack.push(String.valueOf(val2 - val1));
						break;

					case "/":
						// System.out.println("\n\n" + val2 / val1);
						stack.push(String.valueOf(val2 / val1));
						break;

					case "*":
						stack.push(String.valueOf(val2 * val1));
						break;
					case "^":
						// System.out.println("\n###\n" + Math.pow(val2, val1));
						stack.push(String.valueOf(Math.pow(val2, val1)));
						break;
					case "_":
						// System.out.println("\n###\n" + Math.pow(val2, val1));
						stack.push(String.valueOf(Math.sqrt(val2)));
						break;
					default:
						return null;
					}

				} catch (Exception f) {
					val2 = val1;
					if (c.equals("-")) {
						return c + val2;
					} else if (c.equals("_")) {
						return String.valueOf(Math.sqrt(val2));
					}
				}
			}
		}
		// System.out.print("\n\n\n" + stack.pop());
		return stack.pop();
	}

	public static void printTree(String r, String[] tree, File file) {
		String space = "";
		boolean control = true;
		int i = 0;
		int j = -1;
		int rand;
		rand = new Random().nextInt(1000) + 1;
		File file2 = new File(file.getAbsolutePath() + "_outputDraw" + rand + ".txt");
		try {
			Formatter f = new Formatter(file2);
			f.format("Dibujar ", r);
			f.format("=\n %s", r);
			for (String t : tree) {
				// System.out.println(t);
				try {
					if (i == 2) {
						space = space.substring(0, (space.length() - 3));
					}
					f.format("%s%s\n", space, Integer.parseInt(t));//
					System.out.println(space + Integer.parseInt(t));
					i++;
				} catch (Exception e) {
					if (control) {
						j = -2;
						control = false;
					}
					if (j == 2) {
						space = space.substring(0, (space.length() - 3));
					}
					f.format("%s%s\n", space, t); //
					System.out.println(space + t);
					space += "   ";
					i = 0;
					j++;
				}
			}
			JOptionPane.showMessageDialog(null, "Archivo " + file.getName() + "_outputDraw" + rand + ".txt"
					+ " guardado en el mismo directorio al archivo: " + file.getCanonicalPath());
			f.close();
		} catch (Exception e) {
			JOptionPane.showConfirmDialog(null, "No fue posible guardar el arbol en un archivo");
		}
	}
}
