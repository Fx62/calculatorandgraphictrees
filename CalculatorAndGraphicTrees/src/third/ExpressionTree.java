package third;

import java.util.Stack;
import java.util.StringTokenizer;

import javax.swing.JPanel;

public class ExpressionTree {
	Stack<Node> pOperandos = new Stack<Node>();
	Stack<String> pOperadores = new Stack<String>();
	final String empty;
	final String operadores;

	public ExpressionTree() {
		empty = " \t";
		operadores = ")+-*/_^(";
	}

	private Node root;

	public Node getroot() {
		return this.root;
	}

	public void setroot(Node r) {
		this.root = r;
	}

	public boolean contruir(String con) {
		construirArbol(con);
		return true;
	}

	public Node construirArbol(String expresion) {
		StringTokenizer tokenizer;
		String token;

		tokenizer = new StringTokenizer(expresion, empty + operadores, true);
		while (tokenizer.hasMoreTokens()) {
			token = tokenizer.nextToken();
			if (empty.indexOf(token) >= 0)
				;
			else if (operadores.indexOf(token) < 0) {
				Node a;
				pOperandos.push(new Node(token));
			} else if (token.equals(")")) {
				while (!pOperadores.empty() && !pOperadores.peek().equals("(")) {
					guardarSubArbol();
				}
				pOperadores.pop();
			} else {
				if (!token.equals("(") && !pOperadores.empty()) {
					String op = (String) pOperadores.peek();
					while (!op.equals("(") && !pOperadores.empty()
							&& operadores.indexOf(op) >= operadores.indexOf(token)) {
						guardarSubArbol();
						if (!pOperadores.empty())
							op = (String) pOperadores.peek();
					}
				}
				pOperadores.push(token);
			}
		}
		root = (Node) pOperandos.peek();
		while (!pOperadores.empty()) {
			if (pOperadores.peek().equals("(")) {
				pOperadores.pop();
			} else {
				guardarSubArbol();
				root = (Node) pOperandos.peek();
			}
		}
		return root;
	}

	private void guardarSubArbol() {
		Node op2 = (Node) pOperandos.pop();
		Node op1 = (Node) pOperandos.pop();
		pOperandos.push(new Node(op2, pOperadores.pop(), op1));

	}

	public void imprime(Node n) {
		if (n != null) {

			imprime(n.getright());
			System.out.print(n.getdata() + " ");
			imprime(n.getleft());
		}
	}

	public void imprimePos(Node n) {
		if (n != null) {
			imprimePos(n.getleft());
			imprimePos(n.getright());
			System.out.print(n.getdata() + " ");
		}
	}

	public void imprimePre(Node n) {
		if (n != null) {
			System.out.print(n.getdata() + " ");

			imprimePre(n.getright());
			imprimePre(n.getleft());
		}
	}

	public JPanel getdibujo() {
		return new GraphicTree(this);
	}
}
