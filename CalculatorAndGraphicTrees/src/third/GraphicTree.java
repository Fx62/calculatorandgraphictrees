
package third;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.HashMap;

import javax.swing.JPanel;

public class GraphicTree extends JPanel {
	// private Arbol miArbol;
	private ExpressionTree miArbol;
	private HashMap posicionNodos = null;
	private HashMap subtreeSizes = null;
	private boolean dirty = true;
	private int parent2child = 40, child2child = 60;
	private Dimension empty = new Dimension(0, 0);
	private FontMetrics fm = null;

	public GraphicTree(ExpressionTree miArbol) {
		this.miArbol = miArbol;
		this.setBackground(Color.WHITE);
		posicionNodos = new HashMap();
		subtreeSizes = new HashMap();
		dirty = true;
		repaint();
	}

	private void calcularPosiciones() {
		posicionNodos.clear();
		subtreeSizes.clear();
		// Nodo root = this.miArbol.getRaiz();
		Node root = this.miArbol.getroot();
		if (root != null) {
			calcularTamañoSubarbol(root);
			calcularPosicion(root, Integer.MAX_VALUE, Integer.MAX_VALUE, 0);
		}
	}

	private Dimension calcularTamañoSubarbol(Node n) {
		if (n == null)
			return new Dimension(0, 0);

		Dimension ld = calcularTamañoSubarbol(n.getleft());
		Dimension rd = calcularTamañoSubarbol(n.getright());

		int h = fm.getHeight() + parent2child + Math.max(ld.height, rd.height);
		int w = ld.width + child2child + rd.width;

		Dimension d = new Dimension(w, h);
		subtreeSizes.put(n, d);

		return d;
	}

	private void calcularPosicion(Node n, int left, int right, int top) {
		if (n == null)
			return;

		Dimension ld = (Dimension) subtreeSizes.get(n.getleft());
		if (ld == null)
			ld = empty;

		Dimension rd = (Dimension) subtreeSizes.get(n.getright());
		if (rd == null)
			rd = empty;

		int center = 0;

		if (right != Integer.MAX_VALUE)
			center = right - rd.width - child2child / 2;
		else if (left != Integer.MAX_VALUE)
			center = left + ld.width + child2child / 2;
		int width = fm.stringWidth(n.getdata() + "");

		posicionNodos.put(n, new Rectangle(center - width / 2 - 3, top, width + 6, fm.getHeight()));

		calcularPosicion(n.getleft(), Integer.MAX_VALUE, center - child2child / 2,
				top + fm.getHeight() + parent2child);
		calcularPosicion(n.getright(), center + child2child / 2, Integer.MAX_VALUE,
				top + fm.getHeight() + parent2child);
	}

	private void dibujarArbol(Graphics2D g, Node n, int puntox, int puntoy, int yoffs) {
		if (n == null)
			return;

		Rectangle r = (Rectangle) posicionNodos.get(n);
		g.draw(r);
		g.drawString(n.getdata() + "", r.x + 3, r.y + yoffs);

		if (puntox != Integer.MAX_VALUE)

			g.drawLine(puntox, puntoy, (int) (r.x + r.width / 2), r.y);

		dibujarArbol(g, n.getleft(), (int) (r.x + r.width / 2), r.y + r.height, yoffs);
		dibujarArbol(g, n.getright(), (int) (r.x + r.width / 2), r.y + r.height, yoffs);

	}

	public void paint(Graphics g) {
		super.paint(g);
		fm = g.getFontMetrics();

		if (dirty) {
			calcularPosiciones();
			dirty = false;
		}

		Graphics2D g2d = (Graphics2D) g;
		g2d.translate(getWidth() / 2, parent2child);
		dibujarArbol(g2d, this.miArbol.getroot(), Integer.MAX_VALUE, Integer.MAX_VALUE,
				fm.getLeading() + fm.getAscent());
		fm = null;
	}

}
