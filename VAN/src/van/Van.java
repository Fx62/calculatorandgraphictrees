package van;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.naming.spi.DirStateFactory.Result;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.MatteBorder;
import java.awt.Color;

public class Van extends JFrame {

	private JPanel contentPane;
	private JTextField startField;
	private JTextField percentField;
	private JTextField yearField;
	private JTable showTable;
	private long start;
	private byte percent;
	private byte year;
	private static String[] years;
	private static String[] among;
	private static String[] vans;

	public String[] getYears() {
		return years;
	}
	
	public String[] getAmong() {
		return among;
	}

	public String[] getVans() {
		return vans;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Van frame = new Van();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Van() {
		setTitle("Calculo de VAN");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 604, 320);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JLabel lblIngresoInicial = new JLabel("Ingreso inicial");
		GridBagConstraints gbc_lblIngresoInicial = new GridBagConstraints();
		gbc_lblIngresoInicial.anchor = GridBagConstraints.WEST;
		gbc_lblIngresoInicial.insets = new Insets(0, 0, 5, 5);
		gbc_lblIngresoInicial.gridx = 0;
		gbc_lblIngresoInicial.gridy = 0;
		contentPane.add(lblIngresoInicial, gbc_lblIngresoInicial);

		startField = new JTextField();

		GridBagConstraints gbc_startField = new GridBagConstraints();
		gbc_startField.insets = new Insets(0, 0, 5, 0);
		gbc_startField.fill = GridBagConstraints.HORIZONTAL;
		gbc_startField.gridx = 2;
		gbc_startField.gridy = 0;
		contentPane.add(startField, gbc_startField);
		startField.setColumns(10);

		JLabel lblPorcentaje = new JLabel("Porcentaje");
		GridBagConstraints gbc_lblPorcentaje = new GridBagConstraints();
		gbc_lblPorcentaje.anchor = GridBagConstraints.WEST;
		gbc_lblPorcentaje.insets = new Insets(0, 0, 5, 5);
		gbc_lblPorcentaje.gridx = 0;
		gbc_lblPorcentaje.gridy = 1;
		contentPane.add(lblPorcentaje, gbc_lblPorcentaje);

		percentField = new JTextField();
		GridBagConstraints gbc_percentField = new GridBagConstraints();
		gbc_percentField.insets = new Insets(0, 0, 5, 0);
		gbc_percentField.fill = GridBagConstraints.HORIZONTAL;
		gbc_percentField.gridx = 2;
		gbc_percentField.gridy = 1;
		contentPane.add(percentField, gbc_percentField);
		percentField.setColumns(10);

		JLabel lblCantidadDeAos = new JLabel("Cantidad de años");
		GridBagConstraints gbc_lblCantidadDeAos = new GridBagConstraints();
		gbc_lblCantidadDeAos.anchor = GridBagConstraints.WEST;
		gbc_lblCantidadDeAos.insets = new Insets(0, 0, 5, 5);
		gbc_lblCantidadDeAos.gridx = 0;
		gbc_lblCantidadDeAos.gridy = 2;
		contentPane.add(lblCantidadDeAos, gbc_lblCantidadDeAos);

		yearField = new JTextField();
		GridBagConstraints gbc_yearField = new GridBagConstraints();
		gbc_yearField.insets = new Insets(0, 0, 5, 0);
		gbc_yearField.fill = GridBagConstraints.HORIZONTAL;
		gbc_yearField.gridx = 2;
		gbc_yearField.gridy = 2;
		contentPane.add(yearField, gbc_yearField);
		yearField.setColumns(10);

		JCheckBox spends = new JCheckBox("Existen gastos");
		GridBagConstraints gbc_spends = new GridBagConstraints();
		gbc_spends.anchor = GridBagConstraints.WEST;
		gbc_spends.insets = new Insets(0, 0, 5, 5);
		gbc_spends.gridx = 0;
		gbc_spends.gridy = 4;
		contentPane.add(spends, gbc_spends);

		JCheckBox income = new JCheckBox("Ingresos variables");
		GridBagConstraints gbc_income = new GridBagConstraints();
		gbc_income.insets = new Insets(0, 0, 5, 0);
		gbc_income.gridx = 2;
		gbc_income.gridy = 4;
		contentPane.add(income, gbc_income);
		
		JLabel total = new JLabel("");
		GridBagConstraints gbc_total = new GridBagConstraints();
		gbc_total.insets = new Insets(0, 0, 0, 5);
		gbc_total.gridx = 1;
		gbc_total.gridy = 9;
		contentPane.add(total, gbc_total);

		JButton totalButton = new JButton("Calcular");
		totalButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				byte steps = 0;
				try {
					start = Long.parseLong(startField.getText());
					steps++;
				} catch (Exception f) {
					errors("Valor inicial invalido");
				}
				try {
					percent = Byte.parseByte(percentField.getText());
					if(percent > 0 && percent < 100) {
						steps++;
					} else {
						errors("Porcentaje fuera de rango");
					}
				} catch (Exception f) {
					errors("Valor porcentaje invalido");
				}
				try {
					 year = Byte.parseByte(yearField.getText());
					 if(year > 0) {
							steps++;
					 } else {
						 errors("Año fuera de rango");
					 }
				} catch (Exception f) {
					errors("Valor de años invalido");
				}
				if(steps == 3) {
					years = new String[year];
					among = new String[year];
					vans = new String[year];
					long temp1, temp2, temp3, vr = 0;
					if(income.isSelected()) {
						for(int i = 1; i < year + 1; i++ ) {
							temp1 = Long.parseLong(JOptionPane.showInputDialog(null, "Ingreso año" + i, "Ingresos",  
									JOptionPane.DEFAULT_OPTION));
							if(spends.isSelected()) {
								temp2 = Long.parseLong(JOptionPane.showInputDialog(null, "Gastos año" + i, "Gastos",  
										JOptionPane.DEFAULT_OPTION));
								temp1 -= temp2;
								years[i - 1] = "Año " + i;
								temp3 = toLong(percent, i, temp1);
								vr += temp3;
								vans[i - 1] = String.valueOf(temp3);
								among[i - 1] = String.valueOf(temp1);
							} else {
								years[i - 1] = "Año " + i;
								temp3 = toLong(percent, i, temp1);
								vr += temp3;
								vans[i - 1] = String.valueOf(temp3);
								among[i - 1] = String.valueOf(temp1);
							}
						}
					} else {
						temp1 = Long.parseLong(JOptionPane.showInputDialog(null, "Ingreso anual", "Ingresos", 
								JOptionPane.DEFAULT_OPTION));
						if(spends.isSelected()) {
							temp2 = Long.parseLong(JOptionPane.showInputDialog(null, "Gastos anuales", "Gastos",  
									JOptionPane.DEFAULT_OPTION));
							temp1 -= temp2;
							vr = fill(year, percent, temp1);
						} else {
							vr = fill(year, percent, temp1);
						}
					}
					//String[] tempX = new Van().getYears();
					//String[] tempY = new Van().getAmong();
					//String[] tempZ = new Van().getVans();
					DefaultTableModel model = new DefaultTableModel();
					model.setColumnIdentifiers(years);
					model.addRow(years);
					model.addRow(among);
					model.addRow(vans);
					showTable.setModel(model);
					total.setText(showResult(vr, Long.parseLong(startField.getText())));
				} else {
					errors("Favor verificar nuevamente los campos");
				}
			}
		});
		GridBagConstraints gbc_totalButton = new GridBagConstraints();
		gbc_totalButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_totalButton.insets = new Insets(0, 0, 5, 5);
		gbc_totalButton.gridx = 1;
		gbc_totalButton.gridy = 5;
		contentPane.add(totalButton, gbc_totalButton);

		showTable = new JTable();
		showTable.setBorder(new MatteBorder(1, 1, 1, 1, (Color) Color.GRAY));
		GridBagConstraints gbc_showTable = new GridBagConstraints();
		gbc_showTable.gridwidth = 3;
		gbc_showTable.gridheight = 3;
		gbc_showTable.insets = new Insets(0, 0, 5, 0);
		gbc_showTable.fill = GridBagConstraints.BOTH;
		gbc_showTable.gridx = 0;
		gbc_showTable.gridy = 6;
		contentPane.add(showTable, gbc_showTable);
	}
	
	public static void errors(String message) {
		JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public static long toLong(byte percent, long year, long among) {
		double temp = Double.parseDouble(String.format("%.4g%n", (1 / (Math.pow((100 + percent) * .01, year)))));
		System.out.println(temp);
		return (long)(temp * among);
	}
	
	public static long fill(byte year, byte percent, long temp1) {
		long total = 0;
		long temp;
		for(int i = 1; i < year + 1; i++ ) {
			years[i - 1] = "Año " + i;
			temp = toLong(percent, i, temp1);
			vans[i - 1] = String.valueOf(temp);
			total += temp;
			among[i - 1] = String.valueOf(temp1);
			/*System.out.println("Año " + i);
			System.out.println(String.valueOf(toLong(percent, i, temp)));
			System.out.println(String.valueOf(temp));*/
		}
		return total;
	}
	
	public static String showResult(long vr, long start) {
		long result = vr - start;
		return vr + "-" + start + "=" + result;
	}
}
